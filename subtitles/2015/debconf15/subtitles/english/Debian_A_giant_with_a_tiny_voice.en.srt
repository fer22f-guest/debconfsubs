1
00:00:01,621 --> 00:00:05,141
This talk will give an overview of
what the Debian publicity team does

2
00:00:05,340 --> 00:00:07,901
and how they work and how you can
support them.

3
00:00:09,561 --> 00:00:13,101
Please give a warm round of applause to
Cédric Boutillier and his talk

4
00:00:13,381 --> 00:00:15,561
"Debian, a giant with a tiny voice"

5
00:00:15,860 --> 00:00:21,700
[Applause]

6
00:00:22,721 --> 00:00:26,220
I'm sorry, I have a kind of technical
problem.

7
00:00:26,542 --> 00:00:30,941
I don't remember the shortcut for doing
full screen in okular.

8
00:00:35,941 --> 00:00:38,581
Ctrl-Shift-P… ok, thank you.

9
00:00:45,541 --> 00:00:50,581
This is my first DebConf, so I would like
to take this opportunity to present myself

10
00:00:50,881 --> 00:00:56,461
I'm Cédric Boutillier, I'm known as boutil
on IRC and I'm a Debian member

11
00:00:56,640 --> 00:01:02,961
since 2012 and a couple of years before
that, I started contributing to Debian

12
00:01:03,241 --> 00:01:05,121
as a member of the ruby team.

13
00:01:05,421 --> 00:01:11,901
I also joined the french localization team
and I started to translate

14
00:01:12,301 --> 00:01:17,161
some announcements and that's how I became
part of the Publicity team.

15
00:01:18,240 --> 00:01:24,121
What I will talk about today is
the structure of the publicity team,

16
00:01:24,861 --> 00:01:31,961
the various services we are handling in
the team and how you can in fact

17
00:01:32,101 --> 00:01:37,261
get involved in the team and promote
Debian through the Publicity team.

18
00:01:38,541 --> 00:01:41,401
So, what is the structure of the team.

19
00:01:41,541 --> 00:01:47,420
It's a bit complicated because in fact the
publicity in Debian is for the moment

20
00:01:47,620 --> 00:01:51,501
two teams: there is the Press team and the
Publicity team.

21
00:01:53,061 --> 00:01:58,441
The members of the Press team are
delegated by the DPL and

22
00:01:58,620 --> 00:02:03,041
they can speak in the name of the project
when it's needed

23
00:02:03,281 --> 00:02:05,941
to contact for example journalists.

24
00:02:07,101 --> 00:02:13,301
They have a private mail alias
press@debian.org and they serve as a

25
00:02:13,501 --> 00:02:17,221
contact point for journalists and the
outside world.

26
00:02:17,721 --> 00:02:22,581
And there is the Debian Publicity team,
which is much larger, but…

27
00:02:22,962 --> 00:02:29,860
not much larger, larger but not as well
structured as the Press team.

28
00:02:30,301 --> 00:02:35,500
We have a public mailing list,
debian-publicity@lists.debian.org

29
00:02:35,900 --> 00:02:40,061
and an IRC channel, #debian-publicity.

30
00:02:41,140 --> 00:02:45,502
And we should also include in this team
all the people doing reviews,

31
00:02:45,781 --> 00:02:51,821
especially translating our broken english
into proper english − Hello Justin −

32
00:02:53,000 --> 00:02:59,180
and all the translators doing the work to
translate

33
00:02:59,460 --> 00:03:04,501
various announcements in various
languages.

34
00:03:05,981 --> 00:03:10,181
We have also in this Publicity team the
maintainers of the Debian blog,

35
00:03:10,441 --> 00:03:13,980
more on that later, that are also
delegated by the DPL.

36
00:03:15,300 --> 00:03:21,060
And in fact, we should also include the
whole project, because publicity is

37
00:03:21,261 --> 00:03:27,361
the duty of the whole project and
everyone should be concerned by this.

38
00:03:30,981 --> 00:03:36,580
I will now review the various tools we can
have in the team.

39
00:03:38,561 --> 00:03:40,841
First, there are the press announcements.

40
00:03:41,021 --> 00:03:45,600
They are published on the website in the
News/ subsection.

41
00:03:47,041 --> 00:03:54,160
They inform journalists and users of
important changes and they are prepared

42
00:03:54,300 --> 00:03:57,980
by the Press team and the Publicity team

43
00:03:58,581 --> 00:04:03,761
and also with various involved teams when
there are specific changes.

44
00:04:06,501 --> 00:04:16,561
It includes the news for the new releases
and some times also

45
00:04:16,681 --> 00:04:22,301
news that are published in coordination
with other companies or

46
00:04:22,561 --> 00:04:23,841
other projects.

47
00:04:25,221 --> 00:04:29,301
These announcements are a very
official way to communicate

48
00:04:29,540 --> 00:04:30,741
about the project

49
00:04:31,401 --> 00:04:34,501
and on the wiki, at the moment there is
some information about

50
00:04:35,041 --> 00:04:41,781
how you could approach the team to propose
such an announcement.

51
00:04:43,640 --> 00:04:50,201
There is another tool which is used
to publish communication about the project

52
00:04:50,501 --> 00:04:51,781
in a less formal way.

53
00:04:51,981 --> 00:04:56,601
It's the Debian blog, AKA bits.debian.org

54
00:05:00,341 --> 00:05:07,602
It first lived as an unofficial service
under news.debian.net for two years

55
00:05:08,501 --> 00:05:12,761
then it was reopened as an official
service in 2013.

56
00:05:16,360 --> 00:05:26,522
Blog posts that are published there are
less formal,

57
00:05:26,781 --> 00:05:34,101
we can have all kind of announcements
there

58
00:05:34,381 --> 00:05:39,001
so every Debian member has a commit access
to the Git repository

59
00:05:39,220 --> 00:05:44,902
to draft an article which is then reviewed
before the final publishing.

60
00:05:46,540 --> 00:05:53,282
Some teams already have published informal
reports to this blog and

61
00:05:53,441 --> 00:06:00,281
it would be nice if it became something
usual that teams having sprints

62
00:06:00,391 --> 00:06:04,940
could publish informal reports
in this blog.

63
00:06:06,381 --> 00:06:11,881
We have also some Google Summer of Code
announcements and things like that.

64
00:06:16,141 --> 00:06:20,942
Something I know quite well is the Debian
Project News.

65
00:06:22,941 --> 00:06:32,500
This is a newsletter that at its creation was
supposed to be weekly released,

66
00:06:33,281 --> 00:06:43,361
then after some break it was revived as a
bi-monthly newsletter

67
00:06:43,581 --> 00:06:51,781
but at the moment we kind of lack manpower
so it's more or less released once a month.

68
00:06:53,761 --> 00:06:56,681
So what's the structure.

69
00:06:58,921 --> 00:07:07,041
It's available on the website under the
News/weekly/ section of the website.

70
00:07:07,241 --> 00:07:13,661
It's also released as an e-mail on
debian-news and on localized versions

71
00:07:13,841 --> 00:07:16,901
of this newsletter for translations.

72
00:07:17,321 --> 00:07:23,541
It's also available as a RSS feed and
links to the newsletter are also

73
00:07:23,741 --> 00:07:25,521
sent to identi.ca.

74
00:07:26,661 --> 00:07:35,201
It's translated into various languages and
how do we create this newsletter?

75
00:07:35,441 --> 00:07:40,960
We gather various information from mailing
lists, blog posts and

76
00:07:41,241 --> 00:07:44,820
write some short paragraphs about this.

77
00:07:45,041 --> 00:07:50,081
We have also recurrent sections in this
mailing list about

78
00:07:50,240 --> 00:07:56,721
security announcements, interesting new
packages, during freeze time

79
00:07:56,881 --> 00:08:07,881
we publish a summary of the RC bugs statistics
and recently we added some information

80
00:08:08,081 --> 00:08:11,102
about the reproducible builds statistics
too.

81
00:08:13,401 --> 00:08:19,160
A new section that appeared from time to
time in the last issues is the

82
00:08:19,360 --> 00:08:23,201
"Team, what do you do?" section which was
introduced by Donald Norwood.

83
00:08:23,761 --> 00:08:27,362
The principle of this section is to
interview teams.

84
00:08:29,761 --> 00:08:36,621
I think it's a nice way for users and
people interested in Debian in general

85
00:08:36,701 --> 00:08:39,641
to discover the value of various teams,

86
00:08:41,420 --> 00:08:51,561
not only teams doing packaging but teams
doing like cross archive work or

87
00:08:51,720 --> 00:08:55,321
work on other fields of the project.

88
00:08:56,840 --> 00:09:03,081
If your team is invited to answer these
questions, please find some time

89
00:09:03,260 --> 00:09:05,982
to answer to our e-mail and

90
00:09:06,181 --> 00:09:11,881
if your team is interested in
participating in this initiative or

91
00:09:12,042 --> 00:09:16,202
if you know a team that you would be
interested in knowing more about,

92
00:09:17,081 --> 00:09:20,261
please tell us and we'll try to
contact them.

93
00:09:24,141 --> 00:09:27,301
How can you help the Publicity team?

94
00:09:29,581 --> 00:09:33,641
You should consider publicity as a way
way to advertise your work

95
00:09:34,341 --> 00:09:40,640
so you can first join the publicity team
and work directly on

96
00:09:40,941 --> 00:09:45,641
what we are producing: announcements
or this newsletter

97
00:09:45,801 --> 00:09:52,001
by writing, reviewing or translating
articles like for the Debian Project News.

98
00:09:53,340 --> 00:09:59,361
Debian is a very large project and it's
very difficult for us to monitor

99
00:09:59,601 --> 00:10:06,701
all the mailing lists and all the IRC
channels and things like that

100
00:10:06,961 --> 00:10:11,201
so if you can help and collect some
information about what happens

101
00:10:11,380 --> 00:10:12,621
in the project, it's very good.

102
00:10:13,241 --> 00:10:21,681
For example, if you are already a Debian
contributor and you did or you saw

103
00:10:21,841 --> 00:10:24,602
something amazing in the Debian project

104
00:10:24,821 --> 00:10:32,661
you could just send us an e-mail with just
a few lines and a couple of links

105
00:10:32,942 --> 00:10:36,221
and we could include this into the
newsletter.

106
00:10:36,760 --> 00:10:42,541
If you have a package that you are very
happy of,

107
00:10:42,781 --> 00:10:48,051
you are very happy this package entered
the archive and you would like that

108
00:10:48,221 --> 00:10:52,001
a lot of people use this package, you can
also tell us about it

109
00:10:52,142 --> 00:10:58,622
and we will advertise it in the next
Debian Project Newsletter issue.

110
00:11:01,301 --> 00:11:04,641
Working in the Publicity team is also a
good entry point

111
00:11:04,822 --> 00:11:09,761
for people interested in Debian but who
are not contributors yet.

112
00:11:11,241 --> 00:11:16,821
It's a way where people can learn more
about the Debian project.

113
00:11:17,041 --> 00:11:23,940
So, if you are interested in Debian and
you don't know exactly where to start,

114
00:11:24,061 --> 00:11:26,642
it could be a good starting point.

115
00:11:33,382 --> 00:11:35,781
What is the workflow we are using.

116
00:11:35,881 --> 00:11:42,681
Recently, during DebCamp, we migrated
from SVN to Git

117
00:11:42,902 --> 00:11:48,661
so now, all the documents we are handling
are kept in Git repositories.

118
00:11:49,201 --> 00:11:54,781
The Debian Project News, the announcements
and the blog have their own Git repository

119
00:11:55,582 --> 00:12:00,420
Every Debian Member has directly commit
access to these Git repositories

120
00:12:01,041 --> 00:12:09,061
and others can easily get write access by
joining the Publicity team project on Alioth.

121
00:12:09,660 --> 00:12:16,120
Coordination to produce these documents,
annoucements and the DPN,

122
00:12:17,060 --> 00:12:21,921
is usually done through the mailing list
or the IRC channel

123
00:12:22,421 --> 00:12:26,081
and once the announcement or the DPN is
finished

124
00:12:26,360 --> 00:12:34,701
then some calls for review or translation
are sent to translation and localization

125
00:12:34,901 --> 00:12:35,700
mailing lists.

126
00:12:36,861 --> 00:12:40,482
There is more information on the wiki.

127
00:12:42,421 --> 00:12:48,402
There is an other way to publish news
about your work, which is

128
00:12:48,540 --> 00:12:51,561
the Misc Developer News.

129
00:12:51,861 --> 00:12:55,401
It's a wiki page at this address.

130
00:12:56,081 --> 00:13:04,641
This page contains a template you can use
and you can edit the wiki page.

131
00:13:08,481 --> 00:13:13,100
If you are the person adding the fifth
news to this wiki page,

132
00:13:13,770 --> 00:13:19,161
you win the right to collect these five
news and send an e-mail

133
00:13:19,301 --> 00:13:22,961
to debian-devel-announce mailing list
with the five news.

134
00:13:23,280 --> 00:13:29,161
It's quite a light way to send news about
the project,

135
00:13:31,141 --> 00:13:33,640
especially if you are not the fifth one.

136
00:13:38,502 --> 00:13:41,541
Debian is also present on various social
networks.

137
00:13:42,481 --> 00:13:46,921
We have an official identi.ca account

138
00:13:50,660 --> 00:13:52,061
which is represented here.

139
00:13:52,342 --> 00:13:56,021
We're also present on GNU Social, Twitter
and Google+.

140
00:13:59,042 --> 00:14:02,141
There was a special event this year, the
release of Jessie and

141
00:14:02,281 --> 00:14:10,561
we did some live denting on identi.ca and
some messages were sent to Twitter

142
00:14:10,700 --> 00:14:13,740
during the whole weekend, non stop.

143
00:14:14,121 --> 00:14:16,961
We sent like 150 messages.

144
00:14:18,661 --> 00:14:23,561
So if you want to propose a DENT, you can
go directly to

145
00:14:23,741 --> 00:14:29,641
the #debian-publicity IRC channel and send
a proposition with a DENT: prefix

146
00:14:30,641 --> 00:14:35,281
and when this proposition is acknowledged
by someone of the team,

147
00:14:35,442 --> 00:14:40,561
this message will appear on the social
network.

148
00:14:41,100 --> 00:14:45,360
You can also e-mail your proposition to
Debian Publicity.

149
00:14:45,961 --> 00:14:50,361
For example, if you want to publish
messages about DebConf,

150
00:14:50,440 --> 00:14:52,002
you are welcome to do so.

151
00:14:56,161 --> 00:15:03,301
Last tool we have in the Debian Publicity
team is the Debian timeline.

152
00:15:03,581 --> 00:15:08,501
It was created by Chris Lamb and now
maintained by the Publicity team.

153
00:15:09,001 --> 00:15:16,901
It's a web page showing a timeline with
various events related to Debian.

154
00:15:18,021 --> 00:15:23,101
You have the history of all the releases
and various events like sprints,

155
00:15:23,561 --> 00:15:28,901
bug squashing parties, various transitions
and things like that.

156
00:15:32,302 --> 00:15:37,200
All the source code of Debian timeline is
maintained in a Git repository.

157
00:15:37,762 --> 00:15:44,461
So every Debian member can commit to this
timeline and then a publicity member

158
00:15:44,621 --> 00:15:54,721
can push to the server the changes to
update the timeline.

159
00:15:54,981 --> 00:16:01,681
You can look at this timeline and if you
see that some event is missing,

160
00:16:02,561 --> 00:16:09,322
either directly commit to Git or send us
an e-mail to the Debian Publicity team

161
00:16:10,422 --> 00:16:12,620
so we can add this event.

162
00:16:14,761 --> 00:16:19,461
We have also a lot of projects but
currently have no time

163
00:16:19,501 --> 00:16:20,901
to implement them.

164
00:16:21,641 --> 00:16:26,321
Examples of this would be to revive the
"debaday" website

165
00:16:26,441 --> 00:16:30,381
which was presenting a new Debian package
everyday.

166
00:16:31,741 --> 00:16:38,962
We could also try to revive the audio
interviews which are known

167
00:16:39,081 --> 00:16:41,561
under the name of "This Week in Debian",

168
00:16:42,061 --> 00:16:47,222
or subtitle the existing audio interviews
in english

169
00:16:47,521 --> 00:16:51,941
and translate these subtitles into other
languages.

170
00:16:54,121 --> 00:16:58,480
We are also open to new ideas for
recurrent sections

171
00:16:58,521 --> 00:17:00,321
in the Debian Project News.

172
00:17:01,880 --> 00:17:08,441
We could also try to gather some
statistics and

173
00:17:09,021 --> 00:17:12,021
track mentions of Debian on external
websites

174
00:17:13,341 --> 00:17:19,121
to see how Debian is doing from another
point of view.

175
00:17:20,541 --> 00:17:24,161
You're welcome to propose your own idea.

176
00:17:24,681 --> 00:17:29,641
We'll have a BOF this afternoon and you
are welcome to come and

177
00:17:30,022 --> 00:17:32,681
discuss with us your own ideas on that.

178
00:17:34,721 --> 00:17:36,701
Here are some useful links.

179
00:17:37,322 --> 00:17:42,561
For Debian contributors, you have the
mailing and various wiki pages

180
00:17:42,601 --> 00:17:46,821
about the workflow of the team and

181
00:17:48,362 --> 00:17:53,821
for users, these are the mailing list and
social network

182
00:17:54,161 --> 00:17:55,901
where we publish some information.

183
00:17:57,381 --> 00:18:01,821
This is our contact information if you
want to get in touch with us

184
00:18:02,320 --> 00:18:08,321
and please come to the BOF this
afternoon in Amsterdam room at 15:00

185
00:18:08,580 --> 00:18:12,081
We will be happy to discuss with you
everything related to publicity.

186
00:18:12,801 --> 00:18:13,661
Thank you.

187
00:18:14,122 --> 00:18:20,241
[Applause]

188
00:18:21,262 --> 00:18:22,162
[Talk master] Thank you very much.

189
00:18:22,421 --> 00:18:24,881
Are there questions from the audience at
this point?

190
00:18:28,680 --> 00:18:30,821
That doesn't seem to be the case.

191
00:18:36,281 --> 00:18:38,642
[Q] Less a question and more a comment,
really.

192
00:18:39,781 --> 00:18:41,781
Just of highlighting some of
the feedback

193
00:18:41,781 --> 00:18:42,992
we got around the release.

194
00:18:43,462 --> 00:18:49,301
Both the release team and basically
everyone around was

195
00:18:49,521 --> 00:18:52,540
really really impressed with the live
denting and the live tweeting

196
00:18:52,742 --> 00:18:53,621
of the release.

197
00:18:54,061 --> 00:18:58,280
It's something that makes it really
visible for Debian and

198
00:18:58,421 --> 00:19:02,321
when we release things and we're able to
produce that publicity

199
00:19:02,662 --> 00:19:04,521
then it makes Debian a really big thing

200
00:19:04,721 --> 00:19:07,261
so just a huge thanks, really for helping
with that.

201
00:19:07,561 --> 00:19:11,041
I definitely encourage everyone to get
involved with the Publicity team.

202
00:19:12,681 --> 00:19:16,760
It's also a really easy way for people to
get involved,

203
00:19:16,901 --> 00:19:17,881
so if you know anyone who says

204
00:19:18,001 --> 00:19:20,941
"Oh, I'd like help Debian but I'm not very
good at packaging things"

205
00:19:21,121 --> 00:19:24,861
A bit like me as I haven't done anyone in
about 5 years or something.

206
00:19:25,021 --> 00:19:27,901
Getting involved in the Publicity team and
helping out is something that

207
00:19:28,861 --> 00:19:30,362
I definitely really encourage.

208
00:19:30,481 --> 00:19:34,460
It's a really good team, it really needs
help and we can do so much more with it.

209
00:19:43,081 --> 00:19:48,640
[Q] At some point there was a project to
collect a box which could be used

210
00:19:48,681 --> 00:19:52,021
at the exhibits, when Debian goes out and
exhibit.

211
00:19:52,381 --> 00:19:54,961
What is the status now of that?

212
00:19:55,261 --> 00:19:59,380
Did it progress any or do we have a box?
Like in European states where we could

213
00:19:59,641 --> 00:20:02,621
easily distribute to a local party to go,
to bring at the conference

214
00:20:02,961 --> 00:20:05,581
and what it would constitute, I wonder.

215
00:20:06,462 --> 00:20:09,101
Do we have resources for, well…

216
00:20:09,341 --> 00:20:12,141
Many people disagree that "Oh, we
shouldn't have stickers"

217
00:20:12,322 --> 00:20:13,281
or something like that.

218
00:20:13,401 --> 00:20:16,121
I think it's already a visibility, right?

219
00:20:16,261 --> 00:20:20,541
If we have really nice giveaways like all
those stickers we have on our laptops.

220
00:20:20,701 --> 00:20:21,962
Of course we could buy them, right?

221
00:20:22,261 --> 00:20:24,580
But when we go to the conference and
the cost of those

222
00:20:24,680 --> 00:20:29,120
if we print them in bulk, it's
ridiculously small.

223
00:20:29,981 --> 00:20:35,381
I think it would be beneficial if we have
a box, which we could bring

224
00:20:35,521 --> 00:20:40,841
to a conference and has really nice
banners, some nice giveaways.

225
00:20:41,001 --> 00:20:45,641
Maybe not just to throw them around, but
even just for good visitors,

226
00:20:45,901 --> 00:20:48,821
to give a sticker, I think it's nice.

227
00:20:49,241 --> 00:20:51,100
And that's what we do for NeuroDebian
as well.

228
00:20:51,960 --> 00:20:56,561
For those close to us, we give those nice
stickers for the laptop,

229
00:20:56,801 --> 00:20:58,282
they are happy, we are happy.

230
00:20:58,621 --> 00:21:01,621
We have nice exhibit table usually
I think.

231
00:21:02,081 --> 00:21:05,101
So, what is the status there and what
should we do about it?

232
00:21:05,921 --> 00:21:12,880
[A] I think it was handled by the Event
team, but this Event team

233
00:21:13,981 --> 00:21:17,001
doesn't exist anymore, so…

234
00:21:24,081 --> 00:21:26,621
[Maulkin] I think that's the sound 
of someone volunteering.

235
00:21:27,101 --> 00:21:30,161
[laughter]

236
00:21:30,361 --> 00:21:37,801
So, more seriously, I think it was Martin
Zobel wanted a banner and said

237
00:21:37,941 --> 00:21:40,321
"Oh, we should have a banner.
Can we spend the money?"

238
00:21:40,461 --> 00:21:41,582
and I said "Yes, carry on."

239
00:21:42,821 --> 00:21:44,441
He said "But I haven't told you how much
it cost yet."

240
00:21:44,661 --> 00:21:48,400
"Carry on, it's not gonna be like ₤5000,
so just go, make a banner."

241
00:21:49,401 --> 00:21:53,181
But he couldn't get anyone to make the
artwork for him or do anything like that

242
00:21:53,380 --> 00:21:57,761
so he sent out some requests and no one
really was able to do that.

243
00:21:57,981 --> 00:22:01,541
So, if you want to put a box together and
you think we should make stickers,

244
00:22:01,981 --> 00:22:03,921
do that, that sounds good.

245
00:22:04,722 --> 00:22:08,460
[Mr Let's do a box] Banner, we usually use
Ben Armstrong's banner.

246
00:22:09,281 --> 00:22:12,300
And as for volunteering I am
exhibiting

247
00:22:12,441 --> 00:22:13,400
at least twice a year.

248
00:22:13,780 --> 00:22:16,162
So I'm there already.

249
00:22:16,381 --> 00:22:19,320
But box, we have already some box with
some hardware

250
00:22:19,861 --> 00:22:24,320
and if we could reuse it, I wonder,
that would be more efficient.

251
00:22:26,502 --> 00:22:30,122
Do we have any interesting hardware we
should go present as well

252
00:22:30,281 --> 00:22:32,080
that would be nice in that box.

253
00:22:34,461 --> 00:22:37,181
Ok, volunteering. Got it.

254
00:22:37,560 --> 00:22:39,021
[laughter]

255
00:22:39,181 --> 00:22:42,100
[] If you need help with artwork,
just call Valessio.

256
00:22:53,381 --> 00:22:56,342
[Talk master] Ok. Thank you very much.

257
00:22:56,520 --> 00:22:58,681
Let's give the presenter another round
of applause.

258
00:22:59,081 --> 00:23:04,361
[Applause]
